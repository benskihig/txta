import pandas as pd
import numpy as np
from shared.parser import UIMA

uima = UIMA(open(
    '/Users/dipespandey/professional/txta_backup/web/drinking_water_text_10.xmi'))
r = uima.soup.find_all('IGInstitutionalStatement', {
                    "Statementtype": "Regulative Statement"})
c = uima.soup.find_all('IGInstitutionalStatement', {
                    "Statementtype": "Constitutive Statement"})
sofa_string = uima.get_sofa_string()

regulatives = {}
constitutives = {}

regulative_mapper = {
    '(A) Attribute': 'Attribute',
    '(A, prop) Attribute_Property': 'Attribute Property',
    '(D) Deontic': 'Deontic Regulative',
    '(I) Aim': 'Aim',
    '(Bdir) Object_Direct': 'Direct Object',
    '(Bdir, prop) Object_Direct_Property': 'Direct Object Property',
    '(Bind) Object_Indirect': 'Indirect Object',
    '(Bind, prop) Object_Indirect_Property': 'Indirect Object Property',
    '(Cac) Activation Condition': 'Activation Condition Regulative',
    '(Cex) Execution Constraint': 'Execution Constraint Regulative',
    'Or else(Forward reference)': 'Or else(Forward reference)',
    'Or else(Backward reference)': 'Or else(Backward reference)',
    'Logical combination': 'Logical combination Regulative',
    'Statements referenced in logical combination': 'Statements referenced in logical combination Regulative'
}

constitutive_mapper = {
    '(E) Constituted Entity': 'Constituted Entity',
    '(E, prop) Constituted Entity Property': 'Constituted Entity Property',
    '(F) Constitutive Function': 'Function',
    '(P) Constituting Property': 'Constituted Properties',
    '(P, prop) Constituting Property Property': 'Constituted Properties Property',
    '(D) Deontic': 'Deontic Constitutive',
    '(Cac) Activation Condition': 'Activation Condition Constitutive',
    '(Cex) Execution Constraint': 'Execution Constraint Constitutive',
    'Or else(Forward reference)': 'Or else(Forward reference)',
    'Or else(Backward reference)': 'Or else(Backward reference)',
    'Logical combination': 'Logical combination Constitutive',
    'Statements referenced in logical combination': 'Statements referenced in logical combination Regulative'
}

for i in r:
    rs = uima.soup.find_all('IGCoreRegulativeSyntax')
    regulatives[f"{i.get('begin')}:{i.get('end')}"] = []
    for j in rs:
        if int(j.get('begin')) >= int(i.get('begin')) and int(j.get('end')) <= int(i.get('end')):
            regulatives[f"{i.get('begin')}:{i.get('end')}"].append(j)

for i in c:
    cs = uima.soup.find_all('IGCoreConstitutiveSyntax')
    constitutives[f"{i.get('begin')}:{i.get('end')}"] = []
    for j in cs:
        if int(j.get('begin')) >= int(i.get('begin')) and int(j.get('end')) <= int(i.get('end')):
            constitutives[f"{i.get('begin')}:{i.get('end')}"].append(j)

# create the skeleton for the final table
df = pd.DataFrame(columns=['Section Marker', 'Statement No.', 'Raw Statement'] +
                  list(regulative_mapper.values()) + list(constitutive_mapper.values()))

# re-deconstruct the regulatives
for key, values in regulatives.items():
    # get component for each Regulative Statement
    for v in values:
        component_value = v.get('Component')
        if component_value:
            print(regulative_mapper[component_value], f"{v.get('begin')}:{v.get('end')}")
            df = df.append({regulative_mapper[component_value]: sofa_string[int(v.get('begin')):int(v.get('end'))]}, ignore_index=True)

# re-deconstruct the constitutives
for key, values in constitutives.items():
    # get component for each Constitutive Statement
    for v in values:
        component_value = v.get('Component')
        if component_value:
            print(constitutive_mapper[component_value], f"{v.get('begin')}:{v.get('end')}")
            df = df.append(
                {constitutive_mapper[component_value]: sofa_string[int(v.get('begin')):int(v.get('end'))]}, ignore_index=True)

print(sofa_string)
A = df.isnull().values
out = df.values[np.argsort(A, axis=0, kind='mergesort'), np.arange(A.shape[1])]
df_final = pd.DataFrame(out, columns=df.columns)
df_final.to_csv('test.csv')