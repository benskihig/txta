import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent))
from shared.parser import UIMA
import re
import os
import bs4
import pandas as pd
from typing import Any, Dict, List, Union


class TestParser:
    def init(self):
        testfile = Path(__file__).parent.parent.absolute()/Path("test_files/drinking_water_text_5-7.xmi")
        f = open(testfile).read()
        self.uima = UIMA(f)
        self.soup = bs4.BeautifulSoup(f, 'xml')

    def test_get_tags_with_layers(self):
        self.init()
        results = self.uima.get_tags_with_layers()
        assert type(results) == bs4.element.ResultSet
        assert all([tag.get("layer").startswith("webanno.custom") for tag in results]) == True, "One of the layers is not webanno."
        
    def test_get_layer_names(self):
        self.init()
        webanno_set = self.soup.find_all(attrs={"layer": re.compile("webanno.custom*")})
        de_set = self.soup.find_all(attrs={"layer": re.compile("de.tudarmstadt*")})
        test1_result = self.uima.get_layer_names(webanno_set)
        assert all([layer.startswith("webanno.custom") for layer in test1_result]) == False
        test2_result = self.uima.get_layer_names(de_set)        
        assert all([layer.startswith("de.tudarmstadt") for layer in test2_result]) == True
        
