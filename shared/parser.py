import re
import os
import bs4
import pandas as pd
from typing import Any, Dict, List, Union


class UIMA():

    def __init__(self, filehandler):
        self.soup = bs4.BeautifulSoup(filehandler, 'xml')

    def get_tags_with_layers(self) -> bs4.element.ResultSet:
        return self.soup.find_all(attrs={"layer": re.compile("webanno.custom*")})

    def get_custom_tags(self, format=None):
        return NotImplementedError
        
    def get_layer_names(self, tags: bs4.element.ResultSet) -> List[str]:
        return [i.get('layer').replace('webanno.custom.', '') for i in tags]

    def get_all_possible_attributes(self, tags: bs4.element.ResultSet) -> List[str]:
        all_attrs = []
        for i in tags:
            all_attrs.extend(i.attrs.keys())
        return list(set(all_attrs))

    def get_all_custom_tags(self, layer_names):
        all_custom_tags = bs4.element.ResultSet(None)
        for i in layer_names:
            all_custom_tags.extend(self.soup.find_all(i))
        
        return all_custom_tags

    def get_dependent_tags_as_df(self, df):
        df_dependent = df[df.Dependent.notna()]
        for idx, row in df_dependent.iterrows():
            depends_on_row = df[row['Dependent'] == df['xmi:id']]
            df_dependent = df_dependent.append(depends_on_row)
        return df_dependent.sort_values(['b'])

    def convert_tags_to_df(self, tags):
        list_of_dicts = []
        layer=[]
                               
        for i in tags:
            list_of_dicts.append(i.attrs)
            layer.append(i.name)
            
        df = pd.DataFrame(list_of_dicts, index=None)
        df["layer"] = layer  
        return df

    def get_sofa_string(self):
        return self.soup.find('cas:Sofa').get('sofaString')

    def add_sofastring_to_df(self, df):
        sofaString = self.soup.find('cas:Sofa').get('sofaString')
        df['annotated_string'] = df.apply(
            lambda row: sofaString[int(row.begin): int(row.end)], axis=1)
        col = df.pop('annotated_string')
        df.insert(0, 'annotated_string', col)
        return df

    def filter_attributes_from_tags(self, df, attr_filter):
        # maintain a default filter list that can be excluded from the master column list
        # to_include = ['annotated_string', 'StatementEnforcementPerspective', 'ResourceNature', 'Statementtype', 'ResourceType',
        #               'ImplicitComponents', 'ResourceDescriptor', 'Inferredcomponentvalue', 'StatementLevel', 'Component', 'begin', 'end']
        # to_exclude = ['Impliesnegation', 'EmbedsInstitutionalStatement', 'ImpliesNegation', 'Separable',
        #               'Dependent', 'InstitutionalStatement', 'ExternalRegulativeComponentReference', 'PhysicalEntity',
        #               'PrecedenceofComponentCombinations', 'Animacy', 'ExternalConstitutiveComponentReference']
        # filtered_columns = list(
        #     set(df.columns) - set(attr_filter + to_exclude))
        # df = df[to_include]
        df['b'] = df['begin'].astype(int)
        return df.sort_values(['b'])

    def convert(self, df, to: str = 'csv'):
        return NotImplementedError

    def __repr__(self):
        return 'UIMA File tags'

def get_dependent_layers(dependent_tags,all_custom_tags):
    dependent_layers_name = []
    for i in all_custom_tags:
        for j in dependent_tags:
            if ((i['xmi:id']) == (j['Dependent'])):
                dependent_layers_name.append(i.name)
    return dependent_layers_name