import sys
sys.path.append("..")

from shared.parser import UIMA
import numpy as np
from flask import make_response


def get_initial_data(file_path):
    f = open(file_path).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # depedent_layer_name = uima.get_dependent_layers(all_custom_tags,dependent_tags)
    all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    df_with_string = uima.add_sofastring_to_df(df)
    filtered_df = uima.filter_attributes_from_tags(df, ['xmi:id', 'sofa', 'Dependent', 'Governor',
                                                        'Exhaustive', 'LogicalRelationship', 'PrecedenceofComponentCombinations', 'Separable', 'Animacy'])
    sofaString = uima.get_sofa_string()
    return {
        'all_layers': all_layers,
        'all_attributes': all_possible_attributes,
        'df': filtered_df,
        'sofaString': sofaString,
        'file_path': file_path,
        'all_custom_tags': all_custom_tags
    }


def download_as_csv(file_path, layers="all", attributes=None):
    # we filter by layers if the layers attribute is set to something other than 'all' (coma-separated list)
    # otherwise if layers is 'all' we don't do any filtering of the data
    layer_list = [] if layers == "all" else layers.split(",")
    base_output = filter_table_by_layers(file_path, layer_list)
    response = make_response(base_output['df'].to_csv(index=False))
    file_name = file_path.split('/')[-1].split('.')[0]
    response.headers["Content-Disposition"] = f"attachment; filename={file_name}.csv"
    response.headers["Content-Type"] = "text/csv"
    return response


def create_filtered_table(file_path, filters):
    base_output = get_initial_data(file_path)
    return {
        'all_layers': base_output['all_layers'],
        'all_attributes': base_output['all_attributes'],
        'df': base_output['df'][list(set(filters + ['annotated_string', 'layer']))],
        'dfall': base_output['df'],
        'sofaString': base_output['sofaString'],
        'file_path': file_path,
        'all_custom_tags': base_output['all_custom_tags']
    }


def filter_table_by_layers(file_path, layers):
    base_output = get_initial_data(file_path)
    df = base_output['df']
    # we ignore filtering if layers is empty
    if len(layers) > 0:
        query_string = ' | '.join([f"layer=='{i}'" for i in layers])
        df = df.query(query_string)

    return {
        'all_layers': base_output['all_layers'],
        'all_attributes': base_output['all_attributes'],
        'df': df,
        'dfall': base_output['df'],
        'sofaString': base_output['sofaString'],
        'file_path': file_path,
        'all_custom_tags': base_output['all_custom_tags']
    }


def extract_dependent_tags(file_path):
    base_output = get_initial_data(file_path)
    f = open(file_path).read()
    uima = UIMA(f)
    dependent_table = uima.get_dependent_tags_as_df(base_output['df'])
    base_output['df'] = dependent_table
    return base_output


def transpose_by_attribute(file_path, column):
    base_output = get_initial_data(file_path)
    df = base_output['df']
    df[column].replace('', np.nan, inplace=True)
    dfn = df[df[column].notna()][[column, 'annotated_string']]
    return {group: list(members_df['annotated_string'])
              for group, members_df in dfn.groupby(column)}
