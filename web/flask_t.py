try:
    from app import app
    import unittest

except Exception as e:
    print("some Modules are missing{}".format(e))

class FlaskTest(unittest.TestCase):
    #check for response 200
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get("/")
        statusCode = response.status_code
        self.assertEqual(statusCode,200)

    #check for the application return 
    def test_index_content(self):
        tester = app.test_client(self)
        response = tester.get("/")
        self.assertEqual(response.content_type, "text/html; charset=utf-8")



if __name__=="__main__":
    unittest.main()



