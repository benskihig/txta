import os
from flask import Flask, render_template, request
from services import get_initial_data, download_as_csv, \
    create_filtered_table, filter_table_by_layers, extract_dependent_tags, \
    transpose_by_attribute


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def upload_file():

    # File upload
    uploaded_file = request.files['file']
    if uploaded_file.filename != '':
        file_path = os.path.join(os.getcwd(), uploaded_file.filename)
        uploaded_file.save(file_path)

        # Data extraction using the uploaded file
        data = get_initial_data(file_path)
        output = {
            'all_layers': data['all_layers'],
            'all_attributes': data['all_attributes'],
            'df': data['df'],
            'filtered_layers': "all"
        }
        return render_template('index.html', output=output, input=data['sofaString'], file_path=data['file_path'], type='layer')
    return render_template('index.html')


@app.route("/download_file", methods=["GET", "POST"])
def download_file():
    try:
        file_path = request.args.get("file_path")
        layers = request.args.get("layers")
        attributes = request.args.get("attributes")
        return download_as_csv(file_path=file_path, layers=layers, attributes=attributes)
    except Exception as e:
        print(e)
        return f'Error occured: {str(e)}'


@app.route('/filter_attrs', methods=['GET', 'POST'])
def filter_attributes():
    try:
        file_path = request.form.get('file_path')
        filters = request.form.getlist('filter')
        print('file_path', file_path)
        print('filters', filters)
        data = create_filtered_table(file_path, filters)
        output = {
            'all_layers': data['all_layers'],
            'all_attributes': data['all_attributes'],
            'df': data['df'],
            'dfall': data['dfall'],
        }
        return render_template('index.html', output=output, input=data['sofaString'], file_path=data['file_path'])
    except Exception as e:
        print(e)
        return f'Error occured: {str(e)}'


@app.route('/filter_layers', methods=['GET', 'POST'])
def filter_layers():
    try:
        file_path = request.form.get('file_path')
        filters = request.form.getlist('layer-select')
        print('file_path', file_path)
        print('layer_filters', filters)

        data = filter_table_by_layers(file_path, filters)
        output = {
            'all_layers': data['all_layers'],
            'all_attributes': data['all_attributes'],
            'df': data['df'],
            'dfall': data['dfall'],
            'filtered_layers': "all" if len(filters) == 0 else ",".join(filters)
        }
        print(output)
        return render_template('index.html', output=output, input=data['sofaString'], file_path=data['file_path'], type='layer')
    except Exception as e:
        print(e)
        return f'Error occured: {str(e)}'


@app.route('/dependent_tags', methods=['GET', 'POST'])
def get_dependent_tags():
    try:
        file_path = request.args.get("file_path")
        output = extract_dependent_tags(file_path)
        return render_template('index.html', output=output, input=output['sofaString'], file_path=file_path, type='dependent')
    except Exception as e:
        print(e)
        return f'Error occured: {str(e)}'


@app.route('/transpose_table', methods=['GET', 'POST'])
def transpose_table():
    try:
        if request.method == 'POST':
            col_selected = request.form.get('columns')
            file_path = request.form.get('fp')
            grouped = transpose_by_attribute(file_path, col_selected)
            return render_template('transpose_view.html', grouped=grouped, file_path=file_path)
        file_path = request.args.get('file_path')
        output = get_initial_data(file_path)
        return render_template('transpose_view.html', output=output, file_path=file_path)
    except Exception as e:
        print(e)
        return f'Error occured: {str(e)}'

