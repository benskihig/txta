# TXTA
[![Build Status](https://travis-ci.com/benskihig/TXTA.svg?branch=master)](https://travis-ci.com/benskihig/TXTA)

## Running the web app

1. Install Python using conda, or pyenv 

[For Windows](https://www.c-sharpcorner.com/article/steps-to-set-up-a-virtual-environment-for-python-development/)  
[For Linux](https://www.liquidweb.com/kb/creating-virtual-environment-ubuntu-16-04/)

Once the virtualenv is activated

``` bash
$ pip install -r requirements.txt  
$ cd app  
$ flask run  

Then, visit the url: http://127.0.0.1:5000/

```

## CLI Arguments to add

Defaults to config.py

1. --alltags -at -> pull all tags (no transform)
2. --alllayers -al -> pull all layers (done)
3. --customtags -ct -> pull only custom tags
4. --customlayers -cl -> pull out all the custom layers
5. --allattributes -attr -> pull out all the attributes (done)

Also for each of the output commands that return tabular data, add a functionality to save the output as CSV(or other formats)  

6. --customattributes -ca -> pull out the custom tags and represent the attributes in a csv file (done)  
7. --customattribute -ca --filters -fl ['start', 'end', 'Component', 'annotated_string'] -> pull out the custom tags, filter the attributes by the given filter, and represent the attributes in a csv file

## Running the CLI app

--- to be added
