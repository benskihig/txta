import unittest
import wizard
import mock 

class TestInput(unittest.TestCase):
	def test_ifConfigIsEmpty(self):
		self.assertTrue(wizard.configfile["configFile"]) 
		self.assertTrue(wizard.configfile["configLayer"])
		self.assertTrue(wizard.configfile["configCustomAttributes"])
	
	def test_quitting(self):
		with mock.patch('builtins.input', return_value="c"):
			assert yes_or_no() == "Quitter!"

if __name__ == "__main__":
	unittest.main()