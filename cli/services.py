import os
import sys
sys.path.append("..")
import numpy as np
from shared.parser import UIMA
import web.services as webservice


def pull_all_layers(f):
    f = open(f)
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    print(all_layers)
    return all_layers


def save_as_csv(f):
    f = open(f).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    print(df.head())
    outfile = f'{os.getcwd()}/output.csv'
    df.to_csv(outfile, index=False)
    print(f'Saved successfully to {outfile}')


def pull_all_attributes(f):
    f = open(f).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    print(list(df.columns))
    print('All attributes fetched successfully!')
    return list(df.columns)


def filter_layers(file_path, layers, choice=None):
    layers = layers.split(',')
    filtered_by_layers = webservice.filter_table_by_layers(file_path, layers)
    df = filtered_by_layers['df']
    print(df.head())
    if choice == 'Y' or choice == 'y':
        outfile = f'{os.getcwd()}/output.csv'
        df.to_csv(outfile, index=False)
        print(f'Saved successfully to {outfile}')
    return df

def filter_attributes(file_path, attributes, choice=None):
    attributes = attributes.split(',')
    print(attributes)
    f = open(file_path).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    df_filtered = df[attributes]
    print(df_filtered.head())
    if choice == 'Y' or choice == 'y':
        outfile = f'{os.getcwd()}/output.csv'
        df_filtered.to_csv(outfile, index=False)
        print(f'Saved successfully to {outfile}')

def transpose_by_attribute(file_path, column):
    base_output = webservice.get_initial_data(file_path)
    df = base_output['df']
    df[column].replace('', np.nan, inplace=True)
    dfn = df[df[column].notna()][[column, 'annotated_string']]
    output = {group: list(members_df['annotated_string'])
              for group, members_df in dfn.groupby(column)}
    for g, m in output.items():
        print(f'Strings annotated under Column value: {g}')
        print('......................................................................................')
        for i in m:
            print(f'- {i}')
        print('.......................................................................................\n')
        
    return output
