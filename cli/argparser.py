import argparse
import sys
from services import filter_attributes, filter_layers, pull_all_attributes, pull_all_layers, transpose_by_attribute

def parse_args(args):
    parser = argparse.ArgumentParser(description='UIMA IO')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='full path of the input XMI file')

    parser.add_argument('-l', '--layers',
                        nargs='?',
                        const='',
                        help='pull out all the layers from the custom layers')

    parser.add_argument('-s', '--saveascsv',
                        nargs='?',
                        help='pull out the custom tags and represent the attributes in a csv file')

    parser.add_argument('-attr', '--allattributes',
                        nargs='?',
                        const='',
                        help='pull out all the attributes in the custom layers')

    parser.add_argument('-fl', '--filterlayers',
                        nargs='?',
                        help='filter the table based on a comma separated list of layers to be filtered from, eg: l1,l2,l3')


    parser.add_argument('-fa', '--filterattributes',
                        nargs='?',
                        help='filter the table based on a comma separated list of attributes, eg: a1,a2,a3')

    parser.add_argument('-tr', '--transposetable',
                        nargs='?',
                        help='provide an attribute to see which strings have been annotated under the attribute')

    return parser.parse_args()

parser = parse_args(sys.argv[1:])
args = vars(parser)

if args.get('input'):
    input_file = args.get('input')
    if args.get('layers') is not None:
        pull_all_layers(input_file)
    elif args.get('allattributes') is not None:
        pull_all_attributes(input_file)
    elif args.get('filterlayers') is not None:
        layer_filter = args.get('filterlayers')
        choice = input("Do you want to save the output as a csv file? y/n: ")
        filter_layers(input_file, layer_filter, choice)
    elif args.get('filterattributes') is not None:
        attr_filter = args.get('filterattributes')
        choice = input("Do you want to save the output as a csv file? y/n: ")
        filter_attributes(input_file, attr_filter, choice)      
    elif args.get('transposetable') is not None:
        column = args.get('transposetable')
        transpose_by_attribute(input_file, column)