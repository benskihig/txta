import sys
import os
import numpy as np
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent))
sys.path.insert(0, str(Path(__file__).parent.parent.parent))
from services import *

xmi_test_filepath = str(
    Path(__file__).parent.parent.parent
    / Path("shared/test_files/drinking_water_text_5-7.xmi")
)

def test_getlayers():
    print('sending args')
    parsed_layers = pull_all_layers(xmi_test_filepath)
    expected_layers = [
        "IGCheck",
        "IGElement",
        "IGElementRelationship",
        "IGComplexDataStructure",
        "IGCoreConstitutiveComponentRelationship",
        "IGCoreConstitutiveSyntax",
        "IGCoreRegulativeComponentRelationship",
        "IGCoreRegulativeSyntax",
        "IGInstitutionalStatement",
        "IGInstitutionalStatementRelationship",
        "IGPolicyResourceElement",
        "IGPolicyResourceReference",
        "IGPolicyResourceReferenceRelationship",
    ]
    # Assert length
    assert len(parsed_layers) == 13
    # Assert returned array element by element
    assert (np.array(parsed_layers) == np.array(expected_layers)).all() == True

def test_save_as_csv():
    # Remove old output.csv file
    csv_filepath = Path(os.getcwd()) / "output.csv"
    if csv_filepath.is_file():
        os.remove(str(csv_filepath))
    assert csv_filepath.is_file() == False
    save_as_csv(xmi_test_filepath)
    # Above command should create the csv file
    assert csv_filepath.is_file() == True

def test_pull_all_attributes():
    custom_tags = pull_all_attributes(xmi_test_filepath)
    expected_tags = [
        'xmi:id', 'sofa', 'begin', 'end', 'Dependent', 
        'Governor', 'Exhaustive', 'LogicalRelationship', 
        'PrecedenceofComponentCombinations', 'Separable', 'Animacy', 'Component', 
        'EmbedsInstitutionalStatement', 'ImpliesNegation', 'InstitutionalStatement', 
        'Impliesnegation', 'PhysicalEntity', 'Inferredcomponentvalue', 
        'StatementEnforcementPerspective', 'ExternalConstitutiveComponentReference', 
        'ExternalRegulativeComponentReference', 'StatementLevel', 'Statementtype', 
        'ImplicitComponents', 'ResourceDescriptor', 'ResourceType', 'ResourceNature', 'layer']
    # Assert length
    assert len(custom_tags) == 28
    # Assert returned array element by element
    assert (np.array(custom_tags) == np.array(expected_tags)).all() == True

def test_filterlayers_no_data():
    filtered_df = filter_layers(xmi_test_filepath, "IGInstitutionalStatementRelationship")
    # get dataframe index
    index = filtered_df.index
    # get the nr of rows (i.e. filtered data with IGInstitutionalStatementRelationship layer)
    rows = len(index)
    # We expect the above layer to have zero data from the given xmi file
    assert rows == 0

def test_filterlayers_with_data():
    filtered_df = filter_layers(xmi_test_filepath, "IGPolicyResourceElement")
    # get dataframe index
    index = filtered_df.index
    # get the nr of rows (i.e. filtered data with IGPolicyResourceElement layer)
    rows = len(index)
    # We expect three filtered rows with the above layer
    assert rows == 3