import sys
import os
import numpy as np
from pathlib import Path

sys.path.append(str(Path(os.path.abspath(__file__)).parent.parent))
sys.path.append(str(Path(os.path.abspath(__file__)).parent.parent.parent))
from argparser import parse_args

xmi_test_filepath = str(
    Path(__file__).parent.parent.parent
    / Path("/Users/dipespandey/professional/txta_backup/web/drinking_water_text_5-7.xmi")
)


def test_getlayers():
    parser = parse_args(["--layers", xmi_test_filepath])
    expected_layers = [
        "IGCheck",
        "IGElement",
        "IGElementRelationship",
        "IGComplexDataStructure",
        "IGCoreConstitutiveComponentRelationship",
        "IGCoreConstitutiveSyntax",
        "IGCoreRegulativeComponentRelationship",
        "IGCoreRegulativeSyntax",
        "IGInstitutionalStatement",
        "IGInstitutionalStatementRelationship",
        "IGPolicyResourceElement",
        "IGPolicyResourceReference",
        "IGPolicyResourceReferenceRelationship",
    ]
    # Assert length
    assert len(parser.layers) == 13
    # Assert returned array element by element
    assert (np.array(parser.layers) == np.array(expected_layers)).all() == True


def test_custom_attributes():
    # Remove old output.csv file
    csv_filepath = Path(os.getcwd()) / "output.csv"
    if csv_filepath.is_file():
        os.remove(str(csv_filepath))
    assert csv_filepath.is_file() == False
    parse_args(["--customattributes", xmi_test_filepath])
    # Above command should create the csv file
    assert csv_filepath.is_file() == True
    # Count nr of lines in the file
    num_lines = sum(1 for line in open(str(csv_filepath), "r"))
    # Csv file should have 25 lines in total after parsing drinking_water_text_5-7.xmi
    assert num_lines == 25
    # Remove generated test csv file
    os.remove(str(csv_filepath))


def test_pull_attributes():
    parser = parse_args(["--allattributes", xmi_test_filepath])
    expected_attributes = ['xmi:id', 'sofa', 'begin', 'end', 'Dependent', 'Governor', 'Exhaustive', 'LogicalRelationship', 'PrecedenceofComponentCombinations', 'Separable', 'Animacy', 'Component', 'EmbedsInstitutionalStatement', 'ImpliesNegation', 'InstitutionalStatement', 'Impliesnegation', 'PhysicalEntity', 'Inferredcomponentvalue', 'StatementEnforcementPerspective', 'ExternalConstitutiveComponentReference', 'ExternalRegulativeComponentReference', 'StatementLevel', 'Statementtype', 'ImplicitComponents', 'ResourceDescriptor', 'ResourceType', 'ResourceNature', 'layer']
    # Assert Length
    assert len(parser.allattributes) == 28
    # Assert returned array element by element
    assert (
        np.array(parser.allattributes) == np.array(expected_attributes)
    ).all() == True
