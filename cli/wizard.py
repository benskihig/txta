import json
from services import *
import sys
sys.path.append("..")

configfile = {
    "configFile": "file",
    "configLayer": "layer",
    "configCustomAttributes": "term"
}


def configWizard():
    print("Do you want to read (r) config file, or write (w) to config")
    configChoise = input("Input:")
    configChoise = configChoise.lower()
    if configChoise == 'r':
        configRead()
    elif configChoise == "w":
        configWrite()
    else:
        print("error in input")


def configRead():
    with open("config.txt") as config:
        configfile = json.loads(config.read())
    print("reading from config file")
    print("variables found: ")
    print('configFile Path: ', configfile["configFile"])
    print('configFile Layers: ', configfile["configLayer"])
    print('configFile Attributes: ', configfile["configCustomAttributes"])


def configWrite():
    print("read in config values:")
    with open("config.txt") as config:
        configfile = json.loads(config.read())
    cFile = input('file path: ')
    cLayer = input(
        "Please enter a comma separated list of layers you want to add to the config file, eg: L1,L2,L3 :")
    cTerm = input(
        "Please enter a comma separated list of attributes you want to add to the config file, eg: T1,T2,T3: ")
    if cFile:
        configfile["configFile"] = cFile
    if cLayer:
        configfile["configLayer"] = cLayer
    if cTerm:
        configfile["configCustomAttributes"] = cTerm
    f = open("config.txt", "w")
    json.dump(configfile, f)


def useConfig():
    notYetImplemented()


def parseFileWizard():
    notYetImplemented()


def parseShowAll():
    notYetImplemented()


def parseShowLayers():
    notYetImplemented()


def parseShowCustom():
    notYetImplemented()


def parseShowAttributes():
    notYetImplemented()


def analysysWizard():
    notYetImplemented()


def programInfoWizard():
    print("Created by: Alen Bhandari, Dafina Marku, Dipesh Padey, Benjamin Skinstad\n")
    print("how to use this program:")


def notYetImplemented():
    print("not implemented yet")

# relevant tests
# fault tolerance, somewhat modulairty, correctness, need to sanitise input
# need to check for config file, and not trust it
# need to be sure the input the users writes to file is valid


def configparametersetup():
    with open('config.txt', 'w') as outfile:
        json.dump(configfile, outfile)
    print("reading from config file")
    print("variables found: ")
    print(configfile["configFile"])
    print(configfile["configLayer"])
    print(configfile["configCustomTerm"])


def userchoiseSetup():
    print("user input enabled:")
    print("make code for this part")


def viewFile(f):
    print(open(f).read())


def no_args(configfile):
    print("""you do want to:\n 
    read/write from config (c),\n 
    view the file, with options, (v),\n 
    view all layers (l)\n 
    view all attributes (a)\n 
    filter by layers (fl)\n 
    filter by attributes (fa)\n 
    run analytical tools on the file, wip \n 
	view program info (i) """)
    userChoise = input("Input:")
    userChoise = userChoise.lower()
    if userChoise == "c":
        configWizard()
    elif userChoise == "v":
        print('leave empty to use it from config file')
        f = input(
            'Please Enter the input file path: ') or configfile['configFile']
        viewFile(f)
    elif userChoise == "l":
        print('leave empty to use it from config file')
        f = input(
            'Please Enter the input file path: ') or configfile['configFile']
        pull_all_layers(f)
    elif userChoise == "a":
        print('leave empty to use it from config file')
        f = input(
            'Please Enter the input file path: ') or configfile['configFile']
        pull_all_attributes(f)
    elif userChoise == "fl":
        print('leave empty to use it from config file')
        f = input(
            'Please Enter the input file path: ') or configfile['configFile']
        cLayer = input(
            "Please enter a comma separated list of layers you want to add to the config file, eg: L1,L2,L3 :") or configfile['configLayer']
        choice = input("Do you want to save the output as a csv file? y/n: ")
        filter_layers(f, cLayer, choice=choice)
    elif userChoise == "fa":
        print('leave empty to use it from config file')
        f = input(
            'Please Enter the input file path: ') or configfile['configFile']
        cTerm = input(
            "Please enter a comma separated list of attributes you want to add to the config file, eg: T1,T2,T3: ") or configfile['configCustomAttributes']
        choice = input("Do you want to save the output as a csv file? y/n: ")
        filter_attributes(f, cTerm, choice=choice)
    elif userChoise == 'i':
        programInfoWizard()
    else:
        print("error in input")


# read from config
inputFile = ""
inputLayer = ""
inputCustomTerm = ""

# reads numbers of args from cli
numberOfArguments = len(sys.argv)
numberofargs = 0

if __name__ == '__main__':
    with open("config.txt") as tweetfile:
        configfile = json.loads(tweetfile.read())
    no_args(configfile)
