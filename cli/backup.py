import sys
sys.path.append("..")
import numpy as np
import argparse
import os
from shared.parser import UIMA
import web.services as webservice
import json



configfile = {
    "configFile": "file",
    "configLayer": "layer",
    "configCustomAttributes": "term"
}

def configWizard():
	print("Do you want to read (r) config file, or write (w) to config")
	configChoise = input("Input:")
	configChoise = configChoise.lower()
	if configChoise == 'r':
		configRead()

	elif configChoise == "w":
		configWrite()

	else:

		print("error in input") 
	
def configRead(): 
    with open("config.txt") as config:
        configfile = json.loads(config.read())
    print("reading from config file")
    print("variables found: ")
    print(configfile["configFile"])
    print(configfile["configLayer"])
    print(configfile["configCustomAttributes"])
    choice = input('Select one option from the config file: \n \
    a) all layers \n \
    b) all attributes \n \
    c) save xmi as csv \n \
    d) filter xmi by layers \n \
    e) filter xmi by attributes \n \
    ')
    if choice == 'a':
        print(pull_all_layers(configfile['configFile']))
    elif choice == 'b':
        pull_all_attributes(config['configFile'])
    elif choice == 'c':
        save_as_csv(configfile['configFile'])
    elif choice == 'd':
        filters = input('Please enter a comma separated list of layers you want to filter from, the result will be saved as a csv')    
        filter_layers(configfile['configFile'], filters)
    elif choice == 'e':
        filters = input('Please enter a comma separated list of attributes you want to filter from, the result will be saved as a csv')    
        filter_attributes(configfile['configFile'], filters)


def configWrite():
	print("read in config values:")
	configfile["configFile"] = input("file path:")
	configfile["configLayer"] = input("layers:")
	configfile["configCustomTerm"] = input("custom terms:")
	f = open("config.txt","w")
	json.dump(configfile,f)


def useConfig():
	notYetImplemented()

def parseFileWizard():
	notYetImplemented()

def parseShowAll():
	notYetImplemented()

def parseShowLayers():
	notYetImplemented()

def parseShowCustom():
	notYetImplemented()

def parseShowAttributes():
	notYetImplemented()


def analysysWizard():
	notYetImplemented()


def programInfoWizard():
	print("Created by: Alen Bhandari, Dafina Marku, Dipesh Padey, Benjamin Skinstad\n")
	print("how to use this program:")


def notYetImplemented():
	print("not implemented yet")

# relevant tests
# fault tolerance, somewhat modulairty, correctness, need to sanitise input
# need to check for config file, and not trust it
# need to be sure the input the users writes to file is valid


def configparametersetup():
	with open('config.txt', 'w') as outfile:
		json.dump(configfile, outfile)
	print("reading from config file")
	print("variables found: ")
	print(configfile["configFile"])
	print(configfile["configLayer"])
	print(configfile["configCustomTerm"])


def userchoiseSetup():
	print("user input enabled:")
	print("make code for this part")


def pull_all_layers(f):
	f = open(f)
	uima = UIMA(f)
	tags_with_layers = uima.get_tags_with_layers()
	all_layers = uima.get_layer_names(tags_with_layers)
	return all_layers


def save_as_csv(f):
    f = open(f).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    print(df.head())
    outfile = f'{os.getcwd()}/output.csv'
    df.to_csv(outfile, index=False)
    print(f'Saved successfully to {outfile}')


def pull_all_attributes(f):
    f = open(f).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    print(list(df.columns))
    print('All attributes fetched successfully!')


def filter_layers(file_path, layers, outfile=None):
    layers = layers.split(',')
    filtered_by_layers = webservice.filter_table_by_layers(file_path, layers)
    df = filtered_by_layers['df']
    print(df.head())
    outfile = f'{os.getcwd()}/output.csv'
    df.to_csv(outfile, index=False)
    print(f'Saved successfully to {outfile}')


def filter_attributes(file_path, attributes):
    attributes = attributes.split(',')
    print(attributes)
    f = open(file_path).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)
    df_filtered = df[attributes]
    print(df_filtered.head())
    outfile = f'{os.getcwd()}/output.csv'
    df_filtered.to_csv(outfile, index=False)
    print(f'Saved successfully to {outfile}')


def transpose_by_attribute(file_path, column):
    f = open(file_path).read()
    uima = UIMA(f)
    tags_with_layers = uima.get_tags_with_layers()
    all_layers = uima.get_layer_names(tags_with_layers)
    all_custom_tags = uima.get_all_custom_tags(all_layers)
    # all_possible_attributes = uima.get_all_possible_attributes(all_custom_tags)
    df = uima.convert_tags_to_df(
        all_custom_tags).replace(np.nan, '', regex=True)


def no_args():
	print("""you do want to:\n 
    read/write from config (c),\n 
    view the file, with options, (v),\n 
    show the file, using config values (s)\n 
    run analytical tools on the file, wip \n view program info (i) """)
	
	
	userChoise = input("Input:")
	userChoise = userChoise.lower()
	if userChoise == "c":
		configWizard()	
	elif userChoise == "v":
		notYetImplemented()
	elif userChoise == "s":
		notYetImplemented()
	elif userChoise == "a":
		notYetImplemented()
	elif userChoise == "i":
		programInfoWizard()
	else:
		print("error in input")


# read from config
inputFile = ""
inputLayer = ""
inputCustomTerm = ""

# reads numbers of args from cli
numberOfArguments = len(sys.argv)
numberofargs = 0


if __name__ == '__main__':
    with open("config.txt") as tweetfile:
        configfile = json.loads(tweetfile.read())

    parser = argparse.ArgumentParser(description='UIMA IO')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='full path of the input XMI file')

    parser.add_argument('-l', '--layers',
                        nargs='?',
                        help='pull out all the layers from the custom layers')

    parser.add_argument('-s', '--saveascsv',
                        nargs='?',
                        help='pull out the custom tags and represent the attributes in a csv file')

    parser.add_argument('-attr', '--allattributes',
                        nargs='?',
                        help='pull out all the attributes in the custom layers')

    layer_filter = parser.add_argument_group("layer filter")
    layer_filter.add_argument('-fl', '--filterlayers',
                        nargs='?',
                        help='filter the table based on the layers provided')

    layer_filter.add_argument('-ls', '--layerselect',
                        nargs='?',
                        help='a comma separated list of layers to be filtered from')
    
    attribute_filter = parser.add_argument_group("atrribute filter")
    attribute_filter.add_argument('-fa', '--filterattributes',
                        nargs='?',
                        help='filter the table based on the attributes provided')

    attribute_filter.add_argument('-as', '--attributeselect',
                        nargs='?',
                        help='a comma separated list of attributes to filter the whole table from')
    args = vars(parser.parse_args())
    print(args)
    if not any(args.values()):
        no_args()
    elif args['layers']:
        pull_all_layers(args['input'])
    elif args['saveascsv']:
        customattributes = save_as_csv(args['input'])
    elif args['allattributes']:
        pull_all_attributes(args['input'])
    elif args['layerselect']:
        filter_layers(args['input'], args['layerselect'])
    elif args['attributeselect']:
        filter_attributes(args['input'], args['attributeselect'])
